/*
The MIT License (MIT)

Copyright (c) 2016 Nicolas Ruiz Jr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
package tuleestudio;

import tuleestudio.TuleeObject;
import flixel.math.FlxPoint;

class TSUtil {

	public static function copyFloatArray(varName: String, source: TuleeObject): Array<Float> {
		if (source != null) {
			for (key in source.tsLogicMap.keys()) {
	            if (source.tsLogicMap[key].FSMVariableExists(varName)) {
	            	return source.tsLogicMap[key].copyFloatArray(varName);
	            }
	        }
	    }
        return new Array<Float>();
    }

    public static function copyIntArray(varName: String, source: TuleeObject): Array<Int> {
    	if (source != null) {
			for (key in source.tsLogicMap.keys()) {
	            if (source.tsLogicMap[key].FSMVariableExists(varName)) {
	            	return source.tsLogicMap[key].copyIntArray(varName);
	            }
	        }
	    }
        return new Array<Int>();
    }

    public static function copyTuleeObjectArray(varName: String, source: TuleeObject): Array<TuleeObject> {
    	if (source != null) {
			for (key in source.tsLogicMap.keys()) {
	            if (source.tsLogicMap[key].FSMVariableExists(varName)) {
	            	return source.tsLogicMap[key].copyTuleeObjectArray(varName);
	            }
	        }
	    }
        return new Array<TuleeObject>();
    }

    public static function copyPointArray(varName: String, source: TuleeObject): Array<FlxPoint> {
    	if (source != null) {
			for (key in source.tsLogicMap.keys()) {
	            if (source.tsLogicMap[key].FSMVariableExists(varName)) {
	            	return source.tsLogicMap[key].copyPointArray(varName);
	            }
	        }
	    }
        return new Array<FlxPoint>();
    }

    public static function copyStringArray(varName: String, source: TuleeObject): Array<String> {
    	if (source != null) {
			for (key in source.tsLogicMap.keys()) {
	            if (source.tsLogicMap[key].FSMVariableExists(varName)) {
	            	return source.tsLogicMap[key].copyStringArray(varName);
	            }
	        }
	    }
        return new Array<String>();
    }

    public static function copyFloatMap(varName: String, source: TuleeObject): Map<String, Float> {
    	if (source != null) {
			for (key in source.tsLogicMap.keys()) {
	            if (source.tsLogicMap[key].FSMVariableExists(varName)) {
	            	return source.tsLogicMap[key].copyFloatMap(varName);
	            }
	        }
	    }
        return new Map<String, Float>();
    }

    public static function copyIntMap(varName: String, source: TuleeObject): Map<String, Int> {
		if (source != null) {
			for (key in source.tsLogicMap.keys()) {
	            if (source.tsLogicMap[key].FSMVariableExists(varName)) {
	            	return source.tsLogicMap[key].copyIntMap(varName);
	            }
	        }
	    }
        return new Map<String, Int>();
    }

    public static function copyTuleeObjectMap(varName: String, source: TuleeObject): Map<String, TuleeObject> {
		if (source != null) {
			for (key in source.tsLogicMap.keys()) {
	            if (source.tsLogicMap[key].FSMVariableExists(varName)) {
	            	return source.tsLogicMap[key].copyTuleeObjectMap(varName);
	            }
	        }
	    }
        return new Map<String, TuleeObject>();
    }

    public static function copyPointMap(varName: String, source: TuleeObject): Map<String, FlxPoint> {
		if (source != null) {
			for (key in source.tsLogicMap.keys()) {
	            if (source.tsLogicMap[key].FSMVariableExists(varName)) {
	            	return source.tsLogicMap[key].copyPointMap(varName);
	            }
	        }
	    }
        return new Map<String, FlxPoint>();
    }

    public static function copyStringMap(varName: String, source: TuleeObject): Map<String, String> {
		if (source != null) {
			for (key in source.tsLogicMap.keys()) {
	            if (source.tsLogicMap[key].FSMVariableExists(varName)) {
	            	return source.tsLogicMap[key].copyStringMap(varName);
	            }
	        }
	    }
        return new Map<String, String>();
    }

}