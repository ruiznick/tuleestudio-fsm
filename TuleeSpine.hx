/*
The MIT License (MIT)

Copyright (c) 2016 Nicolas Ruiz Jr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
package tuleestudio;

import tuleestudio.TuleeObject;
import tuleestudio.TuleeDataRef;
import tuleestudio.TuleeFSM;

import flixel.FlxObject;
import flixel.FlxBasic;
import flixel.FlxSprite;
import flixel.effects.particles.FlxEmitter;
import flixel.util.FlxPath;
import flixel.text.FlxText;
import flixel.addons.editors.spine.FlxSpine;
import spinehaxe.SkeletonData;
import openfl.Assets;
import flixel.addons.editors.spine.texture.FlixelTextureLoader;
import spinehaxe.SkeletonJson;
import spinehaxe.attachments.AtlasAttachmentLoader;
import spinehaxe.atlas.Atlas;
import flixel.math.FlxPoint;

class TuleeSpine extends FlxSpine implements TuleeObject {

	public var tsLogic: Array<TuleeFSM>;

	public var tsAnims: Map<String, Bool>;

	public var tsDataRef:TuleeDataRef;

	/// Key should be the 'ObjectType'
	public var tsLogicMap: Map<String,TuleeFSM>;

	public static function getSkeletonData(AtlasName:String, AnimationName:String, DataPath:String, Scale:Float = 1):SkeletonData
	{
		try {
			return FlxSpine.readSkeletonData(AtlasName, AnimationName, DataPath, Scale);
		} catch(unknown: Dynamic) {
			return new SkeletonData();
		}
	}

	public function setSkinByName(pName: String) {
		var skin = skeletonData.findSkin(pName);

		if (skin == null) return;

		skeleton.skin = skin;
	}

	public function getBonePositionByName(point: FlxPoint, boneName: String) {
		var bone = skeleton.findBone(boneName);

		if (bone == null) return;

		point.set(bone.worldX, bone.worldY);
	}

	public function addAnimation(animName: String, animDelay: Float) {
		try {
			state.addAnimationByName(0, animName, false, animDelay);
		} catch(unknown: Dynamic) {
			return;
		}
	}

	public function playAnimation(animName: String) {
		if (tsAnims.exists(animName)) {
			setAnimationByName(animName, tsAnims[animName]);
		}
	}

	public function pauseAnimation() {
		state.timeScale = 0;
	}


	public function resumeAnimation() {
		state.timeScale = 1;
	}

	public function hasAnimation(pName: String): Bool {
		return skeletonData.findAnimation(pName) != null;
	}

	public function setAttachment(slotName: String, attachmentName: String) {
		try {
			skeleton.setAttachment(slotName, attachmentName);
		} catch(unknown: Dynamic) {
			return;
		}
	}

	public function setAnimationByName(pName: String, loop: Bool) {
		var curTrack = state.getCurrent(0);

		if (curTrack != null && curTrack.animation != null) {
			if (curTrack.lastTime < curTrack.endTime && curTrack.animation.name == pName) {
				return;
			} 
		}

		var anim = skeletonData.findAnimation(pName);

		if (anim != null) {
			state.timeScale = 1;
			state.setAnimation(0, anim, loop);
		}
	}

    public function new(skelData: SkeletonData) {
        super(skelData);
        tsDataRef = new TuleeDataRef(this);
        Reg.scene.addTSObjectToMap(this);
        tsAnims = new Map<String, Bool>();
    }

    override public function update(elapsed: Float):Void {
		for (tsLogicItem in tsLogic) tsLogicItem.Update();
		super.update(elapsed);
	}

	public function OnCollision(otherObj: TuleeObject):Void {
	}
	
	public function FSMVariableExists(objType: String, varName: String): Bool {
		if (tsLogicMap.exists(objType)) {
			return tsLogicMap[objType].FSMVariableExists(varName);
		}
		return false;
	} 

	public function setFSMValue(objType: String, varName: String, newValue: Dynamic):Bool {
		if (tsLogicMap.exists(objType)) {
			return tsLogicMap[objType].setFSMValue(varName, newValue);
		}
		return false;
	}

    public function getFSMValue(objType: String, varName: String):Dynamic {
    	if (tsLogicMap.exists(objType)) {
    		return tsLogicMap[objType].getFSMValue(varName);
    	}
    	return null;
    }

	public function getObjectType():String {
		return "tuleestudio.TuleeSpine";
	}

	public function variableExists(varName: String): Bool {
		for (key in tsLogicMap.keys()) {
            if (tsLogicMap[key].FSMVariableExists(varName)) {
                return true;
            }
        }
        return false;
	}

	public function setValue(varName: String, newValue: Dynamic):Bool {
		for (key in tsLogicMap.keys()) {
            if (tsLogicMap[key].FSMVariableExists(varName)) {
            	return setFSMValue(key, varName, newValue);
            }
        }
        return false;
	}

	public function getValue(varName: String):Dynamic {
		for (key in tsLogicMap.keys()) {
            if (tsLogicMap[key].FSMVariableExists(varName)) {
            	return getFSMValue(key, varName);
            }
        }
        return null;
	}

	public function containsLogicType(logType: String): Bool {
		for (key in tsLogicMap.keys()) {
            if (tsLogicMap[key].getObjectType() == logType) {
            	return true;
            }
        }
		return false;
	}

	public function jumpAllToState(jumpStateName: String):Void {
		for (key in tsLogicMap.keys()) {
        	tsLogicMap[key].JumpStateByString(jumpStateName);
        }
	}

	public function jumpToState(logType: String, jumpStateName: String):Void {
		for (key in tsLogicMap.keys()) {
            if (tsLogicMap[key].getObjectType() == logType) {
            	tsLogicMap[key].JumpStateByString(jumpStateName);
            	return;
            }
        }
	}

	public function pauseAllLogic():Void {
		for (key in tsLogicMap.keys()) {
        	tsLogicMap[key].isLogicPaused = true;
        }
	}

	public function pauseLogicType(logType: String):Void {
		for (key in tsLogicMap.keys()) {
            if (tsLogicMap[key].getObjectType() == logType) {
            	tsLogicMap[key].isLogicPaused = true;
            	return;
            }
        }
	}

	public function removeLogicType(logType: String):Void {
		var _foundFSM: TuleeFSM = null;
		for (key in tsLogicMap.keys()) {
            if (tsLogicMap[key].getObjectType() == logType) {
            	_foundFSM = tsLogicMap[key];
            	break;
            }
        }

        if (_foundFSM != null) {
        	tsLogic.remove(_foundFSM);
        	tsLogicMap.remove(logType);
        }
	}

	public function removeAllLogic():Void {
		tsLogicMap = new Map<String,TuleeFSM>();
		tsLogic = new Array<TuleeFSM>();
	}

	public function resumeAllLogic():Void {
		for (key in tsLogicMap.keys()) {
        	tsLogicMap[key].isLogicPaused = false;
        }
	}

	public function resumeLogicType(logType: String):Void {
		for (key in tsLogicMap.keys()) {
            if (tsLogicMap[key].getObjectType() == logType) {
            	tsLogicMap[key].isLogicPaused = false;
            	return;
            }
        }
	}

	public function triggerTransition(logType: String, transName: String):Void {
		for (key in tsLogicMap.keys()) {
            if (tsLogicMap[key].getObjectType() == logType) {
            	tsLogicMap[key].TransitionByString(transName);
            	return;
            }
        }
	}

	public function triggerAllTransitions(transName: String):Void {
		for (key in tsLogicMap.keys()) {
        	tsLogicMap[key].TransitionByString(transName);
        }
	}

}
