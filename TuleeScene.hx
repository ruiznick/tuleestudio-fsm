/*
The MIT License (MIT)

Copyright (c) 2016 Nicolas Ruiz Jr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
package tuleestudio;

import flixel.FlxState;
import flixel.FlxG;
import flixel.FlxBasic;
import flixel.group.FlxGroup;
import tuleestudio.TuleeObject;
import tuleestudio.TuleeSprite;

class TuleeScene extends FlxState {

	public var allObjectTypes:Map<String,FlxGroup>;
	public var allTSGroups:Map<String, FlxGroup>;
	public var isSceneReady = false;

    override public function create():Void {
		super.create();

		Reg.scene = this;

		allObjectTypes = new Map<String,FlxGroup>();
		allTSGroups = new Map<String,FlxGroup>();
	}

	override public function destroy():Void {
		super.destroy();
	}

	override public function update(elapsed: Float):Void {
		super.update(elapsed);
	}

	public function recycleInGroup(group: String, ?ObjectClass:Class<FlxBasic>):FlxBasic {
		if (group.length > 0 && allTSGroups.exists(group)) {
			return allTSGroups[group].recycle(ObjectClass);
		}

		return FlxG.state.recycle(ObjectClass);
	}
	
	public function addTSObjectToMap(newObject:TuleeObject) {
		var curObjType = newObject.getObjectType();
		if (allObjectTypes.exists(curObjType)) {
			if (newObject.tsDataRef.flxObject != null) {
				allObjectTypes[curObjType].add(newObject.tsDataRef.flxObject);
			} else if (newObject.tsDataRef.flxBasic != null) {
				allObjectTypes[curObjType].add(newObject.tsDataRef.flxBasic);
			}
		}
	}

	public function tuleeCollision(firstObj: TuleeObject, secondObj: TuleeObject):Void {
		firstObj.OnCollision(secondObj);
		secondObj.OnCollision(firstObj);
	}
}
