/*
The MIT License (MIT)

Copyright (c) 2016 Nicolas Ruiz Jr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
package tuleestudio;

import tuleestudio.TuleeState;
import tuleestudio.TuleeObject;

import flixel.math.FlxPoint;

class TuleeFSM {

    public var curTuleeObject: TuleeObject;

    public var preventTransitioning = false;

    public var currentlyTransitioning = false;

    public var currentlyTransitioningID = -1;

    public var isLogicPaused = false;

    var currentState = -1;

    var fsm:Map<Int,TuleeState>;
    var stateDict:Map<String,Int>;
    var transitionDict:Map<String,Int>;

    var nextState = 0;
    var nextTransition = 0;

    var updateState:Void->Void;

    var onCollisionEvent:TuleeObject->Void;

    public var CurrentState(get,set):Int;
    function get_CurrentState():Int{
        return currentState;
    }

    function set_CurrentState(newState):Int {
        if (currentState == -1) {
            currentState = newState;
            return currentState;
        } else {
            currentState = newState;
            if (fsm.exists(currentState)) {
                updateState = fsm[currentState].UpdateState;
                onCollisionEvent = fsm[currentState].OnCollisionEvent;
            } else {
            updateState = null;
            onCollisionEvent = null;
            }
        }
            return currentState;
    }

    public function new(curTuleeObject: TuleeObject) {
        this.curTuleeObject = curTuleeObject;
        fsm = new Map<Int,TuleeState>();
        stateDict = new Map<String,Int>();
        transitionDict = new Map<String,Int>();
    }

    public function AddState(s:TuleeState) {
        fsm.set(s.stateId, s);

        if (CurrentState == -1) {
            CurrentState = s.stateId;
        }
    }

    public function AddStateByString(s:String):TuleeState {
        var newState = new TuleeState(GetStateID(s),this);
        newState.stateName = s;
        AddState(newState);
        return newState;
    }

    public function AddStateWithAction(s:String,a:Void->Void):TuleeState {
        if (CurrentState == -1) {
            updateState = a;
        }
        var newState = AddStateByString(s);
        newState.UpdateState = a;
        return newState;
    }

    public function TransitionID(t:Int) {
        if (!fsm.exists(CurrentState)) return;
        var s = fsm[CurrentState];
        if (!s.HasTransition(t)) return;
		s.OnLeaving();
        currentlyTransitioningID = t;
        currentlyTransitioning = true;
        if (preventTransitioning) return;
        CurrentState = s.TransitionState(t);
        fsm[CurrentState].OnEntering();
        currentlyTransitioning = false;
    }

    public function SetPreventTransitioning(tValue: Bool) {
        preventTransitioning = tValue;
    }

    public function TransitionByString(t:String) {
        TransitionID(GetTransitionID(t));
    }

    public function JumpState(id:Int) {
        if (!fsm.exists(id)) return;
        var s = fsm[CurrentState];
        s.OnLeaving();
        CurrentState = id;
        fsm[CurrentState].OnEntering();
    }

    public function JumpStateByString(s:String) {
        JumpState(GetStateID(s));
    }

    public function Update() {
        if (isLogicPaused) return;
        if (updateState == null) return;
        if (currentlyTransitioning) {
            TransitionID(currentlyTransitioningID);
        } else {
            updateState();
        }
    }

    public function OnCollision(otherObj: TuleeObject) {
        if (onCollisionEvent == null) return;
        onCollisionEvent(otherObj);
    }

    public function ClearEvents() {
        for (s in fsm) {
            s.ClearEvents();
        }
    }

    public function GetStateID(s:String):Int {
        if (stateDict.exists(s)) return stateDict[s];
        stateDict.set(s,nextState);
        nextState++;
        return stateDict[s];
    }

    public function GetTransitionID(t:String):Int {
        if (transitionDict.exists(t)) return transitionDict[t];
        transitionDict.set(t,nextTransition);
        nextTransition++;
        return transitionDict[t];
    }

    public function GetStateName(i:Int):String {
        for (key in stateDict.keys()) {
            if (stateDict[key] == i) {
                return key;
            }
        }
        return 'StateID ' + i + ' not found.';
    }

    public function GetCurrentStateName():String {
        if (fsm.exists(CurrentState)) {
            return fsm[CurrentState].stateName;
        }
        return '';
    }

    public function GetTransitionName(i:Int):String {
        for (key in transitionDict.keys()) {
            if (transitionDict[key] == i) {
                return key;
            }
        }
        return 'TransitionID' + i + ' not found.';
    }

    public function initFSM() {
    }

    public function IsState(s:String):Bool {
        return (GetStateID(s) == CurrentState);
    }

    public function FSMVariableExists(varName: String): Bool {
        return false;
    }

    public function setFSMValue(varName: String, newValue: Dynamic):Bool {
        return false;
    }

    public function getFSMValue(varName: String):Dynamic {
        return null;
    }

    public function copyFloatArray(varName: String): Array<Float> {
        return new Array<Float>();
    }

    public function copyIntArray(varName: String): Array<Int> {
        return new Array<Int>();
    }

    public function copyTuleeObjectArray(varName: String): Array<TuleeObject> {
        return new Array<TuleeObject>();
    }

    public function copyPointArray(varName: String): Array<FlxPoint> {
        return new Array<FlxPoint>();
    }

    public function copyStringArray(varName: String): Array<String> {
        return new Array<String>();
    }

    public function copyFloatMap(varName: String): Map<String, Float> {
        return new Map<String, Float>();
    }

    public function copyIntMap(varName: String): Map<String, Int> {
        return new Map<String, Int>();
    }

    public function copyTuleeObjectMap(varName: String): Map<String, TuleeObject> {
        return new Map<String, TuleeObject>();
    }

    public function copyPointMap(varName: String): Map<String, FlxPoint> {
        return new Map<String, FlxPoint>();
    }

    public function copyStringMap(varName: String): Map<String, String> {
        return new Map<String, String>();
    }

    public function getObjectType():String {
        return "tuleestudio.TuleeFSM";
    }

}
