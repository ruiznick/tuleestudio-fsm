/*
The MIT License (MIT)

Copyright (c) 2016 Nicolas Ruiz Jr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
package tuleestudio;

import tuleestudio.TuleeDataRef;
import tuleestudio.TuleeFSM;

interface TuleeObject {

	public var tsDataRef:TuleeDataRef;

	public var tsLogic: Array<TuleeFSM>;

	/// Key should be the 'ObjectType'
	public var tsLogicMap: Map<String,TuleeFSM>;
		
	public function OnCollision(otherObj: TuleeObject):Void;
	public function FSMVariableExists(objType: String, varName: String): Bool;
	public function setFSMValue(objType: String, varName: String, newValue: Dynamic):Bool;
    public function getFSMValue(objType: String, varName: String):Dynamic;
	public function getObjectType():String;
	public function variableExists(varName: String): Bool;
	public function setValue(varName: String, newValue: Dynamic):Bool;
	public function getValue(varName: String):Dynamic;
	public function containsLogicType(logType: String): Bool;
	public function jumpAllToState(jumpStateName: String):Void;
	public function jumpToState(logType: String, jumpStateName: String):Void;
	public function pauseAllLogic():Void;
	public function pauseLogicType(logType: String):Void;
	public function removeLogicType(logType: String):Void;
	public function removeAllLogic():Void;
	public function resumeAllLogic():Void;
	public function resumeLogicType(logType: String):Void;
	public function triggerTransition(logType: String, transName: String):Void;
	public function triggerAllTransitions(transName: String):Void;
}