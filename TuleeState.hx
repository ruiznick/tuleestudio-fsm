/*
The MIT License (MIT)

Copyright (c) 2016 Nicolas Ruiz Jr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
package tuleestudio;

import tuleestudio.TuleeFSM;
import tuleestudio.TuleeObject;

class TuleeState {
    public var stateId:Int;
    public var stateName:String = "";

    public var DoBeforeEntering:Void->Void;
    public var DoBeforeLeaving:Void->Void;
    public var UpdateState:Void->Void;
    public var OnCollisionEvent:TuleeObject->Void;

    private var fsm:TuleeFSM;
    private var transitions:Map<Int,Int>;

    public function new(stateId:Int, fsm:TuleeFSM) {
        this.transitions = new Map<Int,Int>();
        this.stateId = stateId;
        this.fsm = fsm;
    }

    public function OnEntering() {
        if (DoBeforeEntering != null) {
            DoBeforeEntering();
        }
    }

    public function OnLeaving() {
        if (DoBeforeLeaving != null) {
            DoBeforeLeaving();
        }
    }

    public function OnCollision(otherObj: TuleeObject) {
    	if (OnCollisionEvent != null) {
    		OnCollisionEvent(otherObj);
    	}
    }

    public function AddTransitionInt(t:Int, s:Int) {
        transitions.set(t, s);
    }

    public function AddTransitionString(t:String, s:String) {
		AddTransitionInt(fsm.GetTransitionID(t), fsm.GetStateID(s));
    }

    public function HasTransition(t:Int):Bool {
        return transitions.exists(t);
    }

    public function TransitionState(t:Int):Int {
        return transitions[t];
    }

    public function ClearEvents() {
        DoBeforeEntering = null;
        DoBeforeLeaving = null;
    }
}
