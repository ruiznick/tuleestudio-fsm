/*
The MIT License (MIT)

Copyright (c) 2016 Nicolas Ruiz Jr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
package tuleestudio;

import tuleestudio.TuleeObject;
import tuleestudio.TuleeSprite;
import tuleestudio.TuleeParticle;
import tuleestudio.TuleeSpine;

import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxBasic;
import flixel.effects.particles.FlxEmitter;
import flixel.util.FlxPath;
import flixel.text.FlxText;

class TuleeDataRef {

	public var flxObject: FlxObject;
	public var flxBasic: FlxBasic;
	public var flxSprite: FlxSprite;
	public var flxEmitter: FlxEmitter;
	public var flxPath: FlxPath;
	public var flxText: FlxText;

	public var tsSprite: TuleeSprite;
	public var tsParticle: TuleeParticle;
	public var tsSpine: TuleeSpine;

	public function new(parentObject: TuleeObject) {
		if (Std.is(parentObject, FlxObject)) {
			flxObject = cast parentObject;
		}

		if (Std.is(parentObject, FlxBasic)) {
			flxBasic = cast parentObject;
		}

		if (Std.is(parentObject, FlxSprite)) {
			flxSprite = cast parentObject;
		}

		if (Std.is(parentObject, FlxEmitter)) {
			flxEmitter = cast parentObject;
		}

		if (Std.is(parentObject, FlxPath)) {
			flxPath = cast parentObject;
		}

		if (Std.is(parentObject, FlxText)) {
			flxText = cast parentObject;
		}

		if (Std.is(parentObject, TuleeSprite)) {
			tsSprite = cast parentObject;
		}

		if (Std.is(parentObject, TuleeParticle)) {
			tsParticle = cast parentObject;
		}

		if (Std.is(parentObject, TuleeSpine)) {
			tsSpine = cast parentObject;
		}
	}	
}
